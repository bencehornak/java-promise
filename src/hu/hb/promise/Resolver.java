package hu.hb.promise;

@FunctionalInterface
public interface Resolver<T> {

    void execute(OnResolve<T> resolve, OnReject reject);

}
