package hu.hb.promise;

@FunctionalInterface
public interface Action<T, R> {

    R execute(T t) throws Exception;

}
